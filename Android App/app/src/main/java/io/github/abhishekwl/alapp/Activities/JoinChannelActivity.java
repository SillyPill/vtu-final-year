package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import java.io.IOException;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class JoinChannelActivity extends AppCompatActivity {

    @BindView(R.id.joinChannelQrCodeView)
    QRCodeReaderView joinChannelQrCodeView;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindString(R.string.batman_qr)
    String batmanServer;

    private Unbinder unbinder;
    private User currentUser;
    private String token;
    private OkHttpClient okHttpClient;
    private MaterialDialog materialDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_channel);

        unbinder = ButterKnife.bind(JoinChannelActivity.this);
        initializeComponents();
        initializeViews();
    }

    private void initializeComponents() {
        currentUser = getIntent().getParcelableExtra("USER");
        token = getIntent().getStringExtra("TOKEN");
        okHttpClient = new OkHttpClient();
    }

    private void initializeViews() {
        initializeQrCodeView();
    }

    private void initializeQrCodeView() {
        joinChannelQrCodeView.setQRDecodingEnabled(true);
        joinChannelQrCodeView.setAutofocusInterval(2000L);
        joinChannelQrCodeView.setBackCamera();
        joinChannelQrCodeView.setOnQRCodeReadListener((text, points) -> new JoinChannel().execute(text));
    }

    @SuppressLint("StaticFieldLeak")
    private class JoinChannel extends AsyncTask<String, Void, Response> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            materialDialog = new MaterialDialog.Builder(JoinChannelActivity.this)
                    .title(R.string.app_name)
                    .content("Attempting to join channel")
                    .progress(true, 0)
                    .titleColorRes(android.R.color.black)
                    .contentColorRes(R.color.colorTextDark)
                    .show();
        }

        @Override
        protected Response doInBackground(String... strings) {
            String qrValue = strings[0];
            String[] values = qrValue.split("-");
            String channelId = values[0];
            String userId = values[1];
            try {
                //MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
		        Log.v("QRDATA", channelId);
                Request joinChannelRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .url(baseServerUrl+"/channels/qrjoin/"+ userId + "/" + channelId)
                        .build();
                return okHttpClient.newCall(joinChannelRequest).execute();
            } catch (IOException ignored) { return null; }
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if (materialDialog.isShowing()) materialDialog.dismiss();
            if (response.isSuccessful()) {
                Toast.makeText(getApplicationContext(), "You have joined the channel", Toast.LENGTH_SHORT).show();
                finish();
            }
            else Toast.makeText(getApplicationContext(), "There has been an error joining the channel", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
