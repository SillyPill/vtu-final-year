package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import io.github.abhishekwl.alapp.Fragments.ConversationsFragment;
import io.github.abhishekwl.alapp.Fragments.DocusignFragment;
import io.github.abhishekwl.alapp.Fragments.EventsFragment;
import io.github.abhishekwl.alapp.Fragments.ProfileFragment;
import io.github.abhishekwl.alapp.Fragments.TodoFragment;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.mainFrameLayout)
    FrameLayout mainFrameLayout;
    @BindView(R.id.mainBottomNavigationView)
    BottomNavigationView mainBottomNavigationView;
    @BindString(R.string.base_server_url)
    String baseServerUrl;

    private Unbinder unbinder;
    private OkHttpClient okHttpClient;
    private String currentUserId, token;
    private FragmentManager fragmentManager;
    private Fragment fragment;
    private MainWebSocketListener mainWebSocketListener;
    private WebSocket webSocket;
    private User currentUser;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location deviceLocation;
    private int repeatFlag = 0;
    private static final int RC_RECORD_AUDIO = 124;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(MainActivity.this);
        initializeComponents();
        initializeViews();
    }

    @SuppressLint("MissingPermission")
    private void initializeComponents() {
        currentUserId = getIntent().getStringExtra("USER_ID");
        token = getIntent().getStringExtra("TOKEN");
        okHttpClient = new OkHttpClient();
        fragmentManager = getSupportFragmentManager();
        //initializeWebSocketClient();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);
        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(location -> deviceLocation = location)
                .addOnFailureListener(e -> Snackbar.make(mainBottomNavigationView, e.getMessage(), Snackbar.LENGTH_LONG).show());
    }

    private void initializeWebSocketClient() {
        Request request = new Request.Builder()
                .addHeader("Authorization", "Bearer "+token)
                .url(baseServerUrl+"/websocket")
                .build();
        mainWebSocketListener = new MainWebSocketListener();
        webSocket = okHttpClient.newWebSocket(request, mainWebSocketListener);
        okHttpClient.dispatcher().executorService().shutdown();
    }

    private void initializeViews() {
        new FetchCurrentUser().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchCurrentUser extends AsyncTask<Void, Void, User> {

        @Override
        protected User doInBackground(Void... voids) {
            try {
                Request userRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .url(baseServerUrl+"/users/"+currentUserId)
                        .build();
                Response userResponse = okHttpClient.newCall(userRequest).execute();
                JSONObject userJson = new JSONObject(Objects.requireNonNull(userResponse.body()).string());
                String userName = userJson.getString("username");
                String usn = userJson.getString("usn");
                userRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .url(baseServerUrl+"/users/"+currentUserId+"/image")
                        .build();
                userResponse = okHttpClient.newCall(userRequest).execute();
                Bitmap userImageBitmap = BitmapFactory.decodeStream(Objects.requireNonNull(userResponse.body()).byteStream());
                currentUser = new User(currentUserId, userName, userImageBitmap);
                currentUser.setUsn(usn);
                return currentUser;
            } catch (IOException | JSONException ignored) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(User user) {
            super.onPostExecute(user);
            initializeBottomNavigationView();
        }
    }

    private void initializeBottomNavigationView() {
        fragment = new ProfileFragment();//
        fragmentManager.beginTransaction().replace(R.id.mainFrameLayout, fragment).commit();
        mainBottomNavigationView.setOnNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.channelsMenuItem:
                    fragment = new ConversationsFragment();
                    break;
                case R.id.todoMenuItem:
                    fragment = new TodoFragment();
                    break;
                case R.id.eventsMenuItem:
                    fragment = new EventsFragment();
                    break;
                case R.id.docuSignMenu:
                    fragment = new DocusignFragment();
                    break;
                default: fragment = new ProfileFragment();
            }
            fragmentManager.beginTransaction().replace(R.id.mainFrameLayout, fragment).commit();
            return true;
        });
    }

    public String getCurrentUserId() {
        return currentUserId;
    }

    public String getToken() {
        return token;
    }


    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    private class MainWebSocketListener extends WebSocketListener {
        private static final int NORMAL_CLOSURE_STATUS = 1000;

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            super.onOpen(webSocket, response);
            try {
                Log.v("WEBSOCKET_OPEN", Objects.requireNonNull(response.body()).string());
            } catch (IOException ignored) { }
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            super.onMessage(webSocket, text);
            Log.v("WEBSOCKET_RECV", text);
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            super.onClosing(webSocket, code, reason);
            webSocket.close(NORMAL_CLOSURE_STATUS,null);
            Log.v("WEBSOCKET_CLOSE",reason);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            super.onFailure(webSocket, t, response);
            Log.v("WEBSOCKET_FAIL", t.getMessage());
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_VOLUME_DOWN) {
            if (repeatFlag==2) {
                new MaterialDialog.Builder(MainActivity.this)
                        .title(R.string.app_name)
                        .content("Are you sure you want to trigger an SOS?")
                        .titleColor(Color.BLACK)
                        .contentColor(Color.BLACK)
                        .positiveText("YES")
                        .negativeText("CANCEL")
                        .positiveColorRes(R.color.colorAccent)
                        .negativeColorRes(R.color.colorTextDark)
                        .onPositive((dialog, which) -> triggerSos())
                        .onNegative((dialog, which) -> dialog.dismiss())
                        .show();
                repeatFlag=0;
            }
            else repeatFlag+=1;
        }
        return true;
    }

    private void triggerSos() {
        String filePath = Environment.getExternalStorageDirectory() + "/recorded_audio.wav";
        String collectedData = "CURRENT USER: "+currentUser.toString()+"\n\nLOCATION: https://www.google.com/maps/search/?api=1&query="+deviceLocation.getLatitude()+","+deviceLocation.getLongitude();
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.putExtra("sms_body", collectedData);
        sendIntent.setType("vnd.android-dir/mms-sms");
        sendIntent.putExtra("address","67905200");
        startActivity(sendIntent);
        AndroidAudioRecorder.with(this)
                .setFilePath(filePath)
                .setColor(getColor(R.color.colorAccent))
                .setRequestCode(RC_RECORD_AUDIO)
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(true)
                .setKeepDisplayOn(true)
                .record();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
