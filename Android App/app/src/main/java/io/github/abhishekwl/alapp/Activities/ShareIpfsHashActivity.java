package io.github.abhishekwl.alapp.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Models.Metadata;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;

public class ShareIpfsHashActivity extends AppCompatActivity {

    @BindView(R.id.shareHashTopLinearLayout)
    LinearLayout shareHashTopLinearLayout;
    @BindView(R.id.shareHashRecyclerView)
    RecyclerView shareHashRecyclerView;
    @BindView(R.id.shareHashProgressBar)
    ProgressBar shareHashProgressBar;

    private Unbinder unbinder;
    private User currentUser;
    private String token;
    private Metadata metadata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_ipfs_hash);

        unbinder = ButterKnife.bind(ShareIpfsHashActivity.this);
        initializeComponents();
        initializeViews();
    }

    private void initializeComponents() {
        currentUser = getIntent().getParcelableExtra("USER");
        token = getIntent().getStringExtra("TOKEN");
        metadata = getIntent().getParcelableExtra("METADATA");
    }

    private void initializeViews() {
        initializeRecyclerView();
    }

    private void initializeRecyclerView() {
        shareHashRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        shareHashRecyclerView.setHasFixedSize(true);
        shareHashRecyclerView.setItemAnimator(new DefaultItemAnimator());
        shareHashRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
