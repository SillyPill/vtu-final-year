package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.R;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SignInActivity extends AppCompatActivity {

    @BindView(R.id.signInUsernameEditText)
    TextInputEditText signInUsernameEditText;
    @BindView(R.id.signInPasswordEditText)
    TextInputEditText signInPasswordEditText;
    @BindView(R.id.signInButton)
    Button signInButton;
    @BindView(R.id.signInSignUpTextView)
    TextView signInSignUpTextView;
    @BindString(R.string.base_server_url)
    String baseServerUrl;

    private Unbinder unbinder;
    private OkHttpClient okHttpClient;
    private MediaType mediaType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        unbinder = ButterKnife.bind(SignInActivity.this);
        initializeComponents();
        initializeViews();
    }

    private void initializeComponents() {
        okHttpClient = new OkHttpClient();
        mediaType = MediaType.parse("application/json; charset=utf-8");
    }

    private void initializeViews() {
        signInUsernameEditText.setText("abhishek");
        signInPasswordEditText.setText("password");
        //TODO: Remove credentials
    }

    @SuppressLint("SetTextI18n")
    @OnClick(R.id.signInButton)
    public void onSignInButtonPress() {
        try {
            String username = Objects.requireNonNull(signInUsernameEditText.getText()).toString();
            String password = Objects.requireNonNull(signInPasswordEditText.getText()).toString();
            JSONObject loginJson = new JSONObject();
            loginJson.put("password", password);
            loginJson.put("login_id", username);
            RequestBody requestBody = RequestBody.create(mediaType, loginJson.toString());
            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(baseServerUrl+"/users/login")
                    .build();
            signInButton.setText("Signing In..");
            signInButton.setEnabled(false);
            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NonNull Call call, @NonNull IOException e) {
                    notifyMessage(e.getMessage());
                }

                @Override
                public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                    try {
                        JSONObject responseJson = new JSONObject(Objects.requireNonNull(response.body()).string());
                        String userId = responseJson.getString("id");
                        String token = response.headers().get("token");
                        Intent mainActivityIntent = new Intent(SignInActivity.this, MainActivity.class);
                        mainActivityIntent.putExtra("USER_ID", userId);
                        mainActivityIntent.putExtra("TOKEN", token);
                        startActivity(mainActivityIntent);
                        finish();
                    } catch (JSONException ignored) { }
                }
            });
        } catch (JSONException ignored) { }
    }

    @OnClick(R.id.signInSignUpTextView)
    public void onSignUpTextViewPress() {
        //TODO: Navigate to Sign Up Activity
    }

    @SuppressLint("SetTextI18n")
    private void notifyMessage(String message) {
        if (!signInButton.isEnabled()) signInButton.setEnabled(true);
        signInButton.setText("Sign In");
        Snackbar.make(signInButton, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
