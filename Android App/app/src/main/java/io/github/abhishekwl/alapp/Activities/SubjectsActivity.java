package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Adapters.SubjectsRecyclerViewAdapter;
import io.github.abhishekwl.alapp.Helpers.RecyclerItemClickListener;
import io.github.abhishekwl.alapp.Models.Channel;
import io.github.abhishekwl.alapp.Models.Post;
import io.github.abhishekwl.alapp.Models.Subject;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SubjectsActivity extends AppCompatActivity {

    @BindView(R.id.subjectsSubjectNameTextView)
    TextView subjectsSubjectNameTextView;
    @BindView(R.id.subjectsTopLinearLayout)
    LinearLayout subjectsTopLinearLayout;
    @BindView(R.id.subjectsMiddleLinearLayout)
    LinearLayout subjectsMiddleLinearLayout;
    @BindView(R.id.subjectsListRecyclerView)
    RecyclerView subjectsListRecyclerView;
    @BindView(R.id.subjectsDefaultSubjectButton)
    FloatingActionButton subjectsDefaultSubjectButton;
    @BindString(R.string.domain)
    String domainUrl;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindString(R.string.global_team_id)
    String globalTeamId;
    @BindView(R.id.subjectsAddSubjectImageView)
    ImageView subjectsAddSubjectImageView;

    private Unbinder unbinder;
    private User currentUser;
    private Channel currentChannel;
    private String token;
    private OkHttpClient okHttpClient;
    private String channelUrl;
    private MaterialDialog materialDialog;
    private ArrayList<Subject> subjectArrayList = new ArrayList<>();
    private SubjectsRecyclerViewAdapter subjectsRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects);

        unbinder = ButterKnife.bind(SubjectsActivity.this);
        initializeComponents();
        initializeViews();
    }

    private void initializeComponents() {
        currentUser = getIntent().getParcelableExtra("USER");
        currentChannel = getIntent().getParcelableExtra("CHANNEL");
        token = getIntent().getStringExtra("TOKEN");
        okHttpClient = new OkHttpClient();
        subjectsRecyclerViewAdapter = new SubjectsRecyclerViewAdapter(subjectArrayList);
    }

    private void initializeViews() {
        if (currentChannel.getType().equalsIgnoreCase("D"))
            subjectsSubjectNameTextView.setText("@".concat(currentChannel.getDisplayName()));
        else subjectsSubjectNameTextView.setText("#".concat(currentChannel.getDisplayName()));
        channelUrl = domainUrl + "/global/channels/" + currentChannel.getDisplayName().trim().replace(" ", "-");
        if (subjectsSubjectNameTextView.getText().toString().startsWith("@"))
            channelUrl = domainUrl + "/global/messages/@" + currentChannel.getDisplayName();
        initializeRecyclerView();
    }

    private void initializeRecyclerView() {
        subjectsListRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        subjectsListRecyclerView.setHasFixedSize(true);
        subjectsListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        subjectsListRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        subjectsListRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Subject selectedSubject = subjectArrayList.get(position);
                Intent threadActivityIntent = new Intent(SubjectsActivity.this, ThreadActivity.class);
                threadActivityIntent.putExtra("USER", currentUser);
                threadActivityIntent.putExtra("CHANNEL", currentChannel);
                threadActivityIntent.putExtra("TOKEN", token);
                threadActivityIntent.putExtra("SUBJECT", selectedSubject);
                startActivity(threadActivityIntent);
            }
        }));
        subjectsListRecyclerView.setAdapter(subjectsRecyclerViewAdapter);
        new FetchAllPostsInChannel().execute(currentChannel.getId());
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchAllPostsInChannel extends AsyncTask<String, Void, ArrayList<Subject>> {

        private ArrayList<Post> postArrayList;

        @Override
        protected ArrayList<Subject> doInBackground(String... strings) {
            try {
                postArrayList = new ArrayList<>();
                Set<String> subjectsSet = new HashSet<>();
                String channelId = strings[0];
                Request postsRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer " + token)
                        .url(baseServerUrl + "/channels/" + channelId + "/posts")
                        .build();
                Response postsResponse = okHttpClient.newCall(postsRequest).execute();
                JSONObject postsRootJson = new JSONObject(Objects.requireNonNull(postsResponse.body()).string());
                JSONArray postsRootOrderJsonArray = postsRootJson.getJSONArray("order");
                JSONObject allPostsJson = postsRootJson.getJSONObject("posts");
                for (int i = 0; i < allPostsJson.length(); i++) {
                    JSONObject postJson = allPostsJson.getJSONObject(postsRootOrderJsonArray.getString(i));
                    String postId = postJson.getString("id");
                    Date postUpdatedAtDate = new Date(postJson.getLong("update_at"));
                    String postUserId = postJson.getString("user_id");
                    String postRootId = postJson.getString("root_id");
                    String postMessage = postJson.getString("message");
                    String postJsonSubject = postJson.getString("subject");
                    if (postJsonSubject != null && !postJsonSubject.isEmpty()) {
                        subjectsSet.add(postJsonSubject.trim().toLowerCase());
                    }
                    Post post = new Post(postId, postUpdatedAtDate, postUserId, channelId, postMessage, postRootId, postJsonSubject, new ArrayList<>());
                    postArrayList.add(post);
                }

                ArrayList<String> subjectNamesArrayList = new ArrayList<>(subjectsSet);
                subjectArrayList.clear();
                for (String subjectName : subjectNamesArrayList) {
                    ArrayList<Post> postsUnderSubjectArrayList = new ArrayList<>();
                    for (Post post : postArrayList) {
                        if (post.getSubject().equalsIgnoreCase(subjectName))
                            postsUnderSubjectArrayList.add(post);
                    }
                    subjectArrayList.add(new Subject(postsUnderSubjectArrayList));
                }

                return subjectArrayList;
            } catch (IOException | JSONException ignored) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Subject> subjects) {
            super.onPostExecute(subjects);
            subjectsRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CreateNewSubject extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            materialDialog = new MaterialDialog.Builder(SubjectsActivity.this)
                    .title(R.string.app_name)
                    .content("Creating subject...")
                    .titleColorRes(android.R.color.black)
                    .contentColorRes(R.color.colorTextDark)
                    .progress(true, 0)
                    .show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {
                String newSubjectName = strings[0];
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                JSONObject requestBodyJson = new JSONObject();
                requestBodyJson.put("channel_id", currentChannel.getId());
                requestBodyJson.put("subject", newSubjectName);
                requestBodyJson.put("message", "New subject created: "+newSubjectName);
                Request createPostRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .post(RequestBody.create(mediaType, requestBodyJson.toString()))
                        .url(baseServerUrl+"/posts")
                        .build();
                okHttpClient.newCall(createPostRequest).execute();
                return null;
            } catch (JSONException | IOException ignored) { return null; }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(materialDialog.isShowing()) {
                materialDialog.dismiss();
                Snackbar.make(subjectsSubjectNameTextView, "Subject created successfully.", Snackbar.LENGTH_LONG).show();
                new FetchAllPostsInChannel().execute(currentChannel.getId());
            }
        }
    }

    @OnClick(R.id.subjectsAddSubjectImageView)
    public void onAddSubjectButtonPressed() {
        materialDialog = new MaterialDialog.Builder(SubjectsActivity.this)
                .title(R.string.app_name)
                .content("Please enter the name for a new subject")
                .titleColorRes(android.R.color.black)
                .contentColorRes(R.color.colorTextDark)
                .input("Subject Name", null, false, (dialog, input) -> {
                    String newSubjectName = input.toString().toLowerCase();
                    new CreateNewSubject().execute(newSubjectName);
                })
                .show();
    }

    @OnClick(R.id.subjectsDefaultSubjectButton)
    public void onDefaultSubjectButtonPress() {
        Intent webViewActivity = new Intent(SubjectsActivity.this, WebViewActivity.class);
        webViewActivity.putExtra("USER", currentUser);
        webViewActivity.putExtra("TOKEN", token);
        webViewActivity.putExtra("WEB_VIEW_URL", channelUrl);
        startActivity(webViewActivity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(SubjectsActivity.this);
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
