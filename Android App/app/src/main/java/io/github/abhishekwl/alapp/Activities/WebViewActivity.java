package io.github.abhishekwl.alapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;

import java.io.File;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import im.delight.android.webview.AdvancedWebView;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;

public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.webviewWebView)
    AdvancedWebView webView;
    @BindString(R.string.domain)
    String domainUrl;

    private User currentUser;
    private String token;
    private String webViewUrl;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_web_view);

        unbinder = ButterKnife.bind(WebViewActivity.this);
        initializeComponents();
        initializeViews();
    }

    private void initializeComponents() {
        currentUser = getIntent().getParcelableExtra("USER");
        token = getIntent().getStringExtra("TOKEN");
        webViewUrl = getIntent().getStringExtra("WEB_VIEW_URL");
    }

    private void initializeViews() {
        initializeWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initializeWebView() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUserAgentString(webSettings.getUserAgentString() + " Web-Atoms-Mobile-WebView");
        webSettings.setDatabaseEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setGeolocationEnabled(true);
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        File dir = getApplicationContext().getExternalCacheDir();
        //webSettings.setAppCacheMaxSize(1024*1024*20);
        webSettings.setAppCachePath(Objects.requireNonNull(dir).getAbsolutePath());
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        CookieManager cookies = CookieManager.getInstance();
        cookies.setAcceptCookie(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(webViewUrl);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        webView.onActivityResult(requestCode, resultCode, intent);
    }
}
