package io.github.abhishekwl.alapp.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;

import net.glxn.qrgen.android.QRCode;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.abhishekwl.alapp.Models.Channel;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.OkHttpClient;

public class ConversationsRecyclerViewAdapter extends RecyclerView.Adapter<ConversationsRecyclerViewAdapter.ConversationViewHolder> {

    private Context rootContext;
    private ArrayList<Channel> channelArrayList;
    private OkHttpClient okHttpClient;
    private User currentUser;
    private String token;

    public ConversationsRecyclerViewAdapter(Context rootContext, ArrayList<Channel> channelArrayList, User currentUser, String token) {
        this.rootContext = rootContext;
        this.channelArrayList = channelArrayList;
        this.okHttpClient = new OkHttpClient();
        this.currentUser = currentUser;
        this.token = token;
    }

    @NonNull
    @Override
    public ConversationsRecyclerViewAdapter.ConversationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.channel_list_item, viewGroup, false);
        return new ConversationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationsRecyclerViewAdapter.ConversationViewHolder conversationViewHolder, int i) {
        Channel channel = channelArrayList.get(i);
        conversationViewHolder.bind(conversationViewHolder.itemView.getContext(), channel);
        conversationViewHolder.itemView.setOnLongClickListener(v -> {
            MaterialDialog materialDialog = new  MaterialDialog.Builder(rootContext)
                    .title(R.string.app_name)
                    .customView(R.layout.qr_code_view, true)
                    .show();
            View rootView = materialDialog.getCustomView();
            ImageView qrCodeImageView = Objects.requireNonNull(rootView).findViewById(R.id.contextMenuQrCodeImageView);
            Bitmap bitmap = QRCode.from(channel.getId().concat("-").concat(currentUser.getId())).bitmap();
            Glide.with(rootView).load(bitmap).into(qrCodeImageView);
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return channelArrayList.size();
    }

    class ConversationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.channelListItemChannelImageView)
        ImageView channelImageView;
        @BindView(R.id.channelListItemChannelNameTextView)
        TextView channelNameTextView;
        @BindView(R.id.channelListItemChannelLatestPostTextView)
        TextView channelLatestPostTextView;
        @BindString(R.string.base_server_url)
        String baseServerUrl;
        @BindString(R.string.placeholder_1)
        String placeholderImageOne;

        ConversationViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Context context, Channel channel) {
            if (channel.getType().equalsIgnoreCase("D")) {
                Glide.with(context).load(channel.getChannelImageBitmap()).into(channelImageView);
                channelNameTextView.setText("@".concat(channel.getDisplayName()));
            }
            else {
                Glide.with(context).load(placeholderImageOne).into(channelImageView);
                channelNameTextView.setText("#".concat(channel.getDisplayName()));
            }
            channelLatestPostTextView.setText(channel.getLastMessage().getText());
        }
    }
}
