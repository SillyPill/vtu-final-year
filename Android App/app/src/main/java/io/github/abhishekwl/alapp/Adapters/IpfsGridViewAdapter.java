package io.github.abhishekwl.alapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.abhishekwl.alapp.Models.Metadata;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;

public class IpfsGridViewAdapter extends RecyclerView.Adapter<IpfsGridViewAdapter.IpfsGridViewHolder> {

    private ArrayList<Metadata> metadataArrayList;
    private User currentUser;

    public IpfsGridViewAdapter(ArrayList<Metadata> metadataArrayList, User currentUser) {
        this.metadataArrayList = metadataArrayList;
        this.currentUser = currentUser;
    }

    @NonNull
    @Override
    public IpfsGridViewAdapter.IpfsGridViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.storage_list_item, viewGroup, false);
        return new IpfsGridViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IpfsGridViewAdapter.IpfsGridViewHolder ipfsGridViewHolder, int i) {
        Metadata metadata = metadataArrayList.get(i);
        ipfsGridViewHolder.bind(ipfsGridViewHolder.itemView.getContext(), metadata);
    }

    @Override
    public int getItemCount() {
        return metadataArrayList.size();
    }

    class IpfsGridViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.storageListItemImageView)
        ImageView itemImageView;
        @BindView(R.id.storageListItemFileNameTextView)
        TextView fileNameTextView;
        @BindString(R.string.ipfs_server_url)
        String ipfsServerUrl;

        IpfsGridViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Context context, Metadata metadata) {
            if (metadata.getContentType().trim().toLowerCase().contains("image")) Glide.with(context).load(ipfsServerUrl+"/fetch?id="+currentUser.getId()+"&hash="+metadata.getFileHash()).into(itemImageView);
            else if (metadata.getContentType().trim().toLowerCase().contains("video")) Glide.with(context).load(metadata.getFileBitmap()).into(itemImageView);
            fileNameTextView.setText(metadata.getFileHash());
        }
    }
}
