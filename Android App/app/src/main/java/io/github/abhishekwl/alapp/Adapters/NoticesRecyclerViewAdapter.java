package io.github.abhishekwl.alapp.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.tiagohm.markdownview.MarkdownView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.github.abhishekwl.alapp.R;

public class NoticesRecyclerViewAdapter extends RecyclerView.Adapter<NoticesRecyclerViewAdapter.NoticeRecyclerViewHolder> {

    private ArrayList<String> stringArrayList;

    public NoticesRecyclerViewAdapter(ArrayList<String> stringArrayList) {
        this.stringArrayList = stringArrayList;
    }

    @NonNull
    @Override
    public NoticesRecyclerViewAdapter.NoticeRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notice_list_item, viewGroup, false);
        return new NoticeRecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoticesRecyclerViewAdapter.NoticeRecyclerViewHolder noticeRecyclerViewHolder, int i) {
        String markdownText = stringArrayList.get(i);
        noticeRecyclerViewHolder.bind(markdownText);
    }

    @Override
    public int getItemCount() {
        return stringArrayList.size();
    }

    class NoticeRecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.noticeListItemMarkdownView)
        MarkdownView markdownView;

        NoticeRecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(String string) {
            markdownView.loadMarkdown(string);
        }
    }
}
