package io.github.abhishekwl.alapp.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Activities.JoinChannelActivity;
import io.github.abhishekwl.alapp.Activities.MainActivity;
import io.github.abhishekwl.alapp.Activities.SubjectsActivity;
import io.github.abhishekwl.alapp.Adapters.ConversationsRecyclerViewAdapter;
import io.github.abhishekwl.alapp.Helpers.RecyclerItemClickListener;
import io.github.abhishekwl.alapp.Models.Channel;
import io.github.abhishekwl.alapp.Models.Message;
import io.github.abhishekwl.alapp.Models.User;
import io.github.abhishekwl.alapp.R;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConversationsFragment extends Fragment {

    @BindView(R.id.conversationsTopLinearLayout)
    LinearLayout conversationsTopLinearLayout;
    @BindView(R.id.conversationsAssistantLinearLayout)
    LinearLayout conversationsAssistantLinearLayout;
    @BindView(R.id.conversationsRecyclerView)
    RecyclerView conversationsRecyclerView;
    @BindView(R.id.conversationsProgressBar)
    ProgressBar conversationsProgressBar;
    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindString(R.string.global_team_id)
    String globalTeamId;
    @BindView(R.id.conversationAddChannelFAB)
    FloatingActionButton conversationAddChannelFAB;
    @BindView(R.id.conversationAddDirectFAB)
    FloatingActionButton conversationAddDirectFAB;
    @BindView(R.id.conversationsAddFAB)
    FloatingActionMenu conversationsAddFAB;

    private View rootView;
    private Unbinder unbinder;
    private OkHttpClient okHttpClient;
    private User currentUser;
    private String token;
    private ConversationsRecyclerViewAdapter conversationsRecyclerViewAdapter;
    private MaterialDialog materialDialog;

    public ConversationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_conversations, container, false);
        unbinder = ButterKnife.bind(ConversationsFragment.this, rootView);
        initializeComponents();
        initializeViews();
        return rootView;
    }

    private void initializeComponents() {
        currentUser = ((MainActivity) Objects.requireNonNull(getActivity())).getCurrentUser();
        token = ((MainActivity) getActivity()).getToken();
        okHttpClient = new OkHttpClient();
    }

    private void initializeViews() {
        initializeRecyclerView();
        new FetchUserChannels().execute();
    }

    private void initializeRecyclerView() {
        conversationsRecyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        conversationsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        conversationsRecyclerView.setHasFixedSize(true);
        conversationsRecyclerView.addItemDecoration(new DividerItemDecoration(rootView.getContext(), DividerItemDecoration.VERTICAL));
    }

    @SuppressLint("StaticFieldLeak")
    private class FetchUserChannels extends AsyncTask<Void, Void, ArrayList<Channel>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (conversationsProgressBar!=null) conversationsProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Channel> doInBackground(Void... voids) {
            try {
                ArrayList<Channel> channelArrayList = new ArrayList<>();
                Request channelsRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer " + token)
                        .url(baseServerUrl + "/users/" + currentUser.getId() + "/teams/" + globalTeamId + "/channels")
                        .build();
                Response channelsResponse = okHttpClient.newCall(channelsRequest).execute();
                JSONArray channelsJson = new JSONArray(Objects.requireNonNull(channelsResponse.body()).string());
                for (int i = 0; i < channelsJson.length(); i++) {
                    JSONObject channelJson = channelsJson.getJSONObject(i);
                    String channelId = channelJson.getString("id");
                    String channelType = channelJson.getString("type");
                    String channelDisplayName = channelJson.getString("display_name");
                    if (channelType.equalsIgnoreCase("D"))
                        channelDisplayName = channelJson.getString("name");
                    String channelPurpose = channelJson.getString("purpose");
                    long channelLastPostAt = channelJson.getLong("last_post_at");
                    long channelTotalMessagesCount = channelJson.getLong("total_msg_count");
                    String channelCreatorId = channelJson.getString("creator_id");
                    Channel channel = new Channel(channelId, channelType, channelDisplayName, channelPurpose, channelLastPostAt, channelTotalMessagesCount, channelCreatorId);
                    channelArrayList.add(channel);
                }
                for (Channel channel : channelArrayList) {
                    User channelUser = null;
                    if (channel.getType().equalsIgnoreCase("D")) {
                        String authorId = channel.getDisplayName();
                        authorId = authorId.substring(authorId.indexOf("_")+2);
                        channelUser = getUserFromUserId(authorId);
                        channel.setDisplayName(channelUser.getUserName());
                    }

                    if (channel.getDisplayName().equalsIgnoreCase("Town Square")) channel.setDisplayName("Channel KSIT");

                    Log.v("CHANNEL", channel.toString());

                    long totalMessageCount = channel.getTotalMessageCount();
                    if (totalMessageCount==0) {
                        Message latestMessage = new Message("bleh", "Start a conversation", currentUser, new Date(), channel.getId(), channel.getType(), null, null);
                        channel.setLastMessage(latestMessage);
                    } else {
                        Request postsInChannelRequest = new Request.Builder()
                                .addHeader("Authorization", "Bearer " + token)
                                .url(baseServerUrl + "/channels/" + channel.getId() + "/posts")
                                .build();
                        Response postsInChannelResponse = okHttpClient.newCall(postsInChannelRequest).execute();
                        JSONObject postsRootJson = new JSONObject(Objects.requireNonNull(postsInChannelResponse.body()).string());
                        JSONArray postsOrderJsonArray = postsRootJson.getJSONArray("order");
                        JSONObject allPostsJson = postsRootJson.getJSONObject("posts");
                        JSONObject latestPostJson = allPostsJson.getJSONObject(postsOrderJsonArray.getString(0));

                        String channelId = channel.getId();
                        String postAuthorId = latestPostJson.getString("user_id");
                        String postId = latestPostJson.getString("id");
                        String postContent = latestPostJson.getString("message");
                        String postType = latestPostJson.getString("type");
                        Date postEditedAtDate = new Date(latestPostJson.getLong("update_at"));
                        User postAuthor = getUserFromUserId(postAuthorId);

                        if (channel.getType().equalsIgnoreCase("D"))
                            channel.setChannelImageBitmap(Objects.requireNonNull(channelUser).getImageBitmap());
                        Message latestMessage = new Message(postId, postContent, postAuthor, postEditedAtDate, channelId, postType, null, null);
                        channel.setLastMessage(latestMessage);
                    }

                }
                return channelArrayList;
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }


        @Override
        protected void onPostExecute(ArrayList<Channel> channelArrayList) {
            super.onPostExecute(channelArrayList);
            if (conversationsProgressBar!=null) conversationsProgressBar.setVisibility(View.GONE);
            if (channelArrayList!=null && !channelArrayList.isEmpty()) {
                conversationsRecyclerViewAdapter = new ConversationsRecyclerViewAdapter(rootView.getContext(), channelArrayList, currentUser, token);
                conversationsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(rootView.getContext(), (view, position) -> {
                    Channel selectedChannel = channelArrayList.get(position);
                    Intent chatActivityIntent = new Intent(rootView.getContext(), SubjectsActivity.class);
                    chatActivityIntent.putExtra("USER", currentUser);
                    chatActivityIntent.putExtra("TOKEN", token);
                    chatActivityIntent.putExtra("CHANNEL", selectedChannel);
                    rootView.getContext().startActivity(chatActivityIntent);
                }));
                //TODO testing onLongPress

                conversationsRecyclerView.setAdapter(conversationsRecyclerViewAdapter);
            }
        }

    }

    private User getUserFromUserId(String uid) throws IOException, JSONException {
        Request userRequest = new Request.Builder()
                .addHeader("Authorization", "Bearer " + token)
                .url(baseServerUrl + "/users/" + uid)
                .build();
        Response userResponse = okHttpClient.newCall(userRequest).execute();
        JSONObject userJson = new JSONObject(Objects.requireNonNull(userResponse.body()).string());
        String username = userJson.getString("username");
        String usn = userJson.getString("usn");
        userRequest = new Request.Builder()
                .addHeader("Authorization", "Bearer " + token)
                .url(baseServerUrl + "/users/" + uid + "/image")
                .build();
        userResponse = okHttpClient.newCall(userRequest).execute();
        Bitmap userImageBitmap = BitmapFactory.decodeStream(Objects.requireNonNull(userResponse.body()).byteStream());
        User user = new User(uid, username, userImageBitmap);
        user.setUsn(usn);
        return user;
    }

    @OnClick(R.id.conversationJoinChannelFAB)
    public void onJoinChannelButtonPress() {
        Intent qrCodeJoinChannelIntent = new Intent(rootView.getContext(), JoinChannelActivity.class);
        qrCodeJoinChannelIntent.putExtra("USER", currentUser);
        qrCodeJoinChannelIntent.putExtra("TOKEN", token);
        rootView.getContext().startActivity(qrCodeJoinChannelIntent);
    }

    @OnClick(R.id.conversationAddDirectFAB)
    public void onDirectMessagesAddFab() {
        new ShowUserNames().execute();
    }

    @SuppressLint("StaticFieldLeak")
    private class ShowUserNames extends AsyncTask<Void, Void, ArrayList<String>> {

        private ArrayList<User> userArrayList;
        private ArrayList<String> userNamesArrayList;

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {
            try {
                userArrayList = new ArrayList<>();
                userNamesArrayList = new ArrayList<>();
                Request usersInTeamRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .url(baseServerUrl+"/users?in_team="+globalTeamId)
                        .build();
                Response usersInTeamResponse = okHttpClient.newCall(usersInTeamRequest).execute();
                JSONArray usersInTeamJsonArray = new JSONArray(Objects.requireNonNull(usersInTeamResponse.body()).string());
                userArrayList.clear();
                for (int i=0; i<usersInTeamJsonArray.length(); i++) {
                    JSONObject userJson = usersInTeamJsonArray.getJSONObject(i);
                    String userId = userJson.getString("id");
                    String userName = userJson.getString("username");
                    User user = getUserFromUserId(userId);
                    userArrayList.add(user);
                    userNamesArrayList.add(userName);
                }
                return userNamesArrayList;
            } catch (IOException | JSONException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            super.onPostExecute(strings);
            for (int i=0; i<strings.size(); i++) if (strings.get(i).equals(currentUser.getUserName())) strings.remove(i);
            materialDialog = new MaterialDialog.Builder(rootView.getContext())
                    .title(R.string.app_name)
                    .content("Who do you want to converse with?")
                    .titleColorRes(android.R.color.black)
                    .contentColorRes(R.color.colorTextDark)
                    .items(strings)
                    .itemsColorRes(R.color.colorTextDark)
                    .itemsCallback((dialog, itemView, position, text) -> {
                        User selectedUser = userArrayList.get(position);
                        new CreateDirectMessageChannel().execute(selectedUser);
                    })
                    .show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CreateDirectMessageChannel extends AsyncTask<User, Void, Void> {

        @Override
        protected Void doInBackground(User... users) {
            try {
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                User selectedUser = users[0];
                JSONArray usersInChannelArray = new JSONArray();
                usersInChannelArray.put(0,selectedUser.getId());
                usersInChannelArray.put(1, currentUser.getId());
                RequestBody requestBody = RequestBody.create(mediaType, usersInChannelArray.toString());
                Request createDmChannelRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer "+token)
                        .url(baseServerUrl + "/channels/direct")
                        .post(requestBody)
                        .build();
                Response dmChannelResponse = okHttpClient.newCall(createDmChannelRequest).execute();
                Log.v("DM_RESP", Objects.requireNonNull(dmChannelResponse.body()).string());
            } catch (JSONException | IOException e) { }
            return null;
        }
    }

    @OnClick(R.id.conversationAddChannelFAB)
    public void onChannelAddFabPress() {
        MaterialDialog materialDialog = new MaterialDialog.Builder(rootView.getContext())
                .title("KSIT")
                .content("Enter channel name")
                .input("Channel name", null, false, (dialog, input) -> {
                    String enteredChannelName = input.toString().trim();
                    Log.v("CHANNEL_NAME", enteredChannelName);
                    //TODO: API call to create new channel
                    new CreateChannelc().execute(enteredChannelName);

		    try {
			    MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                String channelUniqueName = enteredChannelName.replace(' ', '-').toLowerCase().trim();
			    JSONObject newChannelDetails = new JSONObject();

			    newChannelDetails.put("team_id", globalTeamId);
			    newChannelDetails.put("name", channelUniqueName);
			    newChannelDetails.put("display_name", enteredChannelName);
			    newChannelDetails.put("type", "O");

			    Request createChannelRequest = new Request.Builder()
				    .addHeader("Authorization", "Bearer " + token)
				    .post(RequestBody.create(mediaType, newChannelDetails.toString()))
				    .url(baseServerUrl + "/channels" )
				    .build();
			    Response createChannelResponse = okHttpClient.newCall(createChannelRequest).execute();

			    Log.v("CREATE_CHANNEL", Objects.requireNonNull(createChannelResponse.body()).toString());
		    } catch ( Exception e ) { Log.e("EXCEPTION", e.toString() ); }
                })
                .show();
    }

    private class CreateChannelc extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... strings) {
            try {
                String enteredChannelName = strings[0];
                MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
                String channelUniqueName = enteredChannelName.replace(' ', '-').toLowerCase().trim();
                JSONObject newChannelDetails = new JSONObject();

                newChannelDetails.put("team_id", globalTeamId);
                newChannelDetails.put("name", channelUniqueName);
                newChannelDetails.put("display_name", enteredChannelName);
                newChannelDetails.put("type", "O");

                Request createChannelRequest = new Request.Builder()
                        .addHeader("Authorization", "Bearer " + token)
                        .post(RequestBody.create(mediaType, newChannelDetails.toString()))
                        .url(baseServerUrl + "/channels" )
                        .build();
                Response createChannelResponse = okHttpClient.newCall(createChannelRequest).execute();

                Log.v("CREATE_CHANNEL", Objects.requireNonNull(createChannelResponse.body()).toString());
            } catch ( Exception e ) { Log.e("EXCEPTION", e.toString() ); }
            return null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(ConversationsFragment.this, rootView);
        new FetchUserChannels().execute();
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
