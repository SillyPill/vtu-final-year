package io.github.abhishekwl.alapp.Fragments;


import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.github.abhishekwl.alapp.Activities.MainActivity;
import io.github.abhishekwl.alapp.Adapters.NoticesRecyclerViewAdapter;
import io.github.abhishekwl.alapp.R;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DocusignFragment extends Fragment {


    @BindString(R.string.base_server_url)
    String baseServerUrl;
    @BindString(R.string.batman_notices)
    String noticeUrl;
    @BindView(R.id.webviewDocusign)
    WebView webViewDocusign;

    private ArrayList<String> noticesArrayList = new ArrayList<>();
    private View rootView;
    private Unbinder unbinder;
    private OkHttpClient okHttpClient;
    private NoticesRecyclerViewAdapter noticesRecyclerViewAdapter;

    private WebView webView;

    public DocusignFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_docusign, container, false);
        unbinder = ButterKnife.bind(DocusignFragment.this, rootView);
        initializeComponents();
        initializeViews();
        return rootView;
    }

    private void initializeComponents() {
        noticesRecyclerViewAdapter = new NoticesRecyclerViewAdapter(noticesArrayList);
        okHttpClient = new OkHttpClient();
    }

    private void initializeViews() {
        initializeRecyclerView();
    }

    private void initializeRecyclerView() {
       // TODO
        WebSettings webSettings = webViewDocusign.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setUserAgentString(webSettings.getUserAgentString() + " Web-Atoms-Mobile-WebView");
        webSettings.setDatabaseEnabled(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setGeolocationEnabled(true);
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        //webSettings.setAppCacheMaxSize(1024*1024*20);
        //webSettings.setAppCachePath(Objects.requireNonNull(dir).getAbsolutePath());
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        CookieManager cookies = CookieManager.getInstance();
        cookies.setAcceptCookie(true);
      //  webView.setWebViewClient(new WebViewClient());
        webViewDocusign.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event){

                // This is Filter
                if( event.getAction() != KeyEvent.ACTION_DOWN)
                    return true;

                if ( keyCode == KeyEvent.KEYCODE_BACK){
                    if (webViewDocusign.canGoBack()){
                        webViewDocusign.goBack();
                    } else {
                        ((MainActivity)getActivity()).onBackPressed();
                    }
                    return true;
                }
                return false;
            }
        });

        webViewDocusign.setWebChromeClient(new WebChromeClient());
        webViewDocusign.loadUrl("https://account-d.docusign.com/");
    }


    @Override
    public void onStart() {
        super.onStart();
        unbinder = ButterKnife.bind(DocusignFragment.this, rootView);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
