package io.github.abhishekwl.alapp.Models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.ArrayList;
import java.util.List;

public class Channel implements Parcelable, IDialog<Message> {

    private String id;
    private String type;
    private String displayName;
    private String purpose;
    private long lastPostAt;
    private long totalMessageCount;
    private String channelCreateId;
    private ArrayList<Message> messageArrayList;
    private Bitmap channelImageBitmap;

    public Channel(String id, String type, String displayName, String purpose, long lastPostAt, long totalMessageCount, String channelCreateId) {
        this.id = id;
        this.type = type;
        this.displayName = displayName;
        this.purpose = purpose;
        this.lastPostAt = lastPostAt;
        this.totalMessageCount = totalMessageCount;
        this.channelCreateId = channelCreateId;
        this.messageArrayList = new ArrayList<>();
    }

    public Channel(String id, String type, String displayName, String purpose, long lastPostAt, long totalMessageCount, String channelCreateId, Bitmap channelImageBitmap) {
        this.id = id;
        this.type = type;
        this.displayName = displayName;
        this.purpose = purpose;
        this.lastPostAt = lastPostAt;
        this.totalMessageCount = totalMessageCount;
        this.channelCreateId = channelCreateId;
        this.messageArrayList = new ArrayList<>();
        this.channelImageBitmap = channelImageBitmap;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getDialogPhoto() {
        return null;
    }

    @Override
    public String getDialogName() {
        return displayName;
    }

    @Override
    public List<? extends IUser> getUsers() {
        return null;
    }

    @Override
    public Message getLastMessage() {
        return messageArrayList.isEmpty()? null : messageArrayList.get(0);
    }

    @Override
    public void setLastMessage(Message message) {
        if(messageArrayList.isEmpty()) messageArrayList.add(message);
    }

    @Override
    public int getUnreadCount() {
        return 0;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public long getLastPostAt() {
        return lastPostAt;
    }

    public void setLastPostAt(long lastPostAt) {
        this.lastPostAt = lastPostAt;
    }

    public long getTotalMessageCount() {
        return totalMessageCount;
    }

    public void setTotalMessageCount(long totalMessageCount) {
        this.totalMessageCount = totalMessageCount;
    }

    public String getChannelCreateId() {
        return channelCreateId;
    }

    public void setChannelCreateId(String channelCreateId) {
        this.channelCreateId = channelCreateId;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", displayName='" + displayName + '\'' +
                ", purpose='" + purpose + '\'' +
                ", lastPostAt=" + lastPostAt +
                ", totalMessageCount=" + totalMessageCount +
                ", channelCreateId='" + channelCreateId + '\'' +
                ", messageArrayList=" + messageArrayList +
                '}';
    }

    public ArrayList<Message> getMessageArrayList() {
        return messageArrayList;
    }

    public void setMessageArrayList(ArrayList<Message> messageArrayList) {
        this.messageArrayList = messageArrayList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.type);
        dest.writeString(this.displayName);
        dest.writeString(this.purpose);
        dest.writeLong(this.lastPostAt);
        dest.writeLong(this.totalMessageCount);
        dest.writeString(this.channelCreateId);
        dest.writeTypedList(this.messageArrayList);
    }

    private Channel(Parcel in) {
        this.id = in.readString();
        this.type = in.readString();
        this.displayName = in.readString();
        this.purpose = in.readString();
        this.lastPostAt = in.readLong();
        this.totalMessageCount = in.readLong();
        this.channelCreateId = in.readString();
        this.messageArrayList = in.createTypedArrayList(Message.CREATOR);
    }

    public static final Creator<Channel> CREATOR = new Creator<Channel>() {
        @Override
        public Channel createFromParcel(Parcel source) {
            return new Channel(source);
        }

        @Override
        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

    public Bitmap getChannelImageBitmap() {
        return channelImageBitmap;
    }

    public void setChannelImageBitmap(Bitmap channelImageBitmap) {
        this.channelImageBitmap = channelImageBitmap;
    }
}
