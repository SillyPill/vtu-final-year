package io.github.abhishekwl.alapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

public class Post implements Parcelable {

    private String id;
    private Date editedAt;
    private String userId;
    private String channelId;
    private String message;
    private String rootId;
    private String subject;
    private ArrayList<String> fileIds;

    public Post(String id, Date editedAt, String userId, String channelId, String message, String rootId, String subject, ArrayList<String> fileIds) {
        this.id = id;
        this.editedAt = editedAt;
        this.userId = userId;
        this.channelId = channelId;
        this.message = message;
        this.rootId = rootId;
        this.subject = subject;
        this.fileIds = fileIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getEditedAt() {
        return editedAt;
    }

    public void setEditedAt(Date editedAt) {
        this.editedAt = editedAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRootId() {
        return rootId;
    }

    public void setRootId(String rootId) {
        this.rootId = rootId;
    }

    public ArrayList<String> getFileIds() {
        return fileIds;
    }

    public void setFileIds(ArrayList<String> fileIds) {
        this.fileIds = fileIds;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeLong(this.editedAt != null ? this.editedAt.getTime() : -1);
        dest.writeString(this.userId);
        dest.writeString(this.channelId);
        dest.writeString(this.message);
        dest.writeString(this.rootId);
        dest.writeString(this.subject);
        dest.writeStringList(this.fileIds);
    }

    private Post(Parcel in) {
        this.id = in.readString();
        long tmpEditedAt = in.readLong();
        this.editedAt = tmpEditedAt == -1 ? null : new Date(tmpEditedAt);
        this.userId = in.readString();
        this.channelId = in.readString();
        this.message = in.readString();
        this.rootId = in.readString();
        this.subject = in.readString();
        this.fileIds = in.createStringArrayList();
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    @Override
    public String toString() {
        return "Post{" +
                "id='" + id + '\'' +
                ", editedAt=" + editedAt +
                ", userId='" + userId + '\'' +
                ", channelId='" + channelId + '\'' +
                ", message='" + message + '\'' +
                ", rootId='" + rootId + '\'' +
                ", subject='" + subject + '\'' +
                ", fileIds=" + fileIds +
                '}';
    }
}
