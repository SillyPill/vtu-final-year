package io.github.abhishekwl.alapp.Models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import com.stfalcon.chatkit.commons.models.IUser;

import java.io.ByteArrayOutputStream;

public class User implements IUser, Parcelable {

    private String id;
    private String userName;
    private String imageUrl;
    private Bitmap imageBitmap;
    private String usn;

    public User(String id, String userName, String imageUrl) {
        this.id = id;
        this.userName = userName;
        this.imageUrl = imageUrl;
    }

    public User(String id, String userName, Bitmap imageBitmap) {
        this.id = id;
        this.userName = userName;
        this.imageBitmap = imageBitmap;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return imageUrl;
    }

    @Override
    public String getAvatar() {
        return BitMapToString(imageBitmap);
    }

    private String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos = new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", userName='" + userName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", imageBitmap=" + imageBitmap +
                ", usn='" + usn + '\'' +
                '}';
    }

    public String getUsn() {
        return usn;
    }

    public void setUsn(String usn) {
        this.usn = usn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.userName);
        dest.writeString(this.imageUrl);
        dest.writeParcelable(this.imageBitmap, flags);
        dest.writeString(this.usn);
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.userName = in.readString();
        this.imageUrl = in.readString();
        this.imageBitmap = in.readParcelable(Bitmap.class.getClassLoader());
        this.usn = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
