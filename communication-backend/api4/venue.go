// Copyright (c) 2017-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

package api4

import (
	// "fmt"
	// "io"
	// "io/ioutil"
	"net/http"
	// "strconv"
	// "time"

	// "github.com/mattermost/mattermost-server/app"
	// "github.com/mattermost/mattermost-server/mlog"
	"github.com/mattermost/mattermost-server/model"
)

func (api *API) InitVenue () {
	api.BaseRoutes.Venue.Handle("", api.ApiHandler(listVenues)).Methods("GET")
	api.BaseRoutes.Venue.Handle("/add", api.ApiHandler(AddVenue)).Methods("POST")

	// api.BaseRoutes.Users.Handle("", api.ApiHandler(createUser)).Methods("POST")
	// api.BaseRoutes.Users.Handle("", api.ApiSessionRequired(getUsers)).Methods("GET")
}

func listVenues(c *Context, w http.ResponseWriter, r *http.Request) {

	if c.Err != nil {
		return
	}

	// No permission check required

	var venue []*model.Venue
	var err *model.AppError

	if venue, err = c.App.GetVenueList(); err != nil {
		c.Err = err
		return
	}

	c.App.UpdateLastActivityAtIfNeeded(c.Session)
	w.Header().Set(model.HEADER_ETAG_SERVER, "etag")
	w.Write([]byte(model.VenueListToJson(venue)))
}

func AddVenue(c *Context, w http.ResponseWriter, r *http.Request) {
	venue := model.VenueFromJson(r.Body)
	if venue == nil {
		c.SetInvalidParam("venue")
		return
	}

	// tokenId := r.URL.Query().Get("t")
	// inviteId := r.URL.Query().Get("iid")

	// No permission check required

	var rvenue *model.Venue
	var err *model.AppError

	if rvenue, err = c.App.CreateVenue(venue); err != nil {
		c.Err = err
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(rvenue.ToJson()))


	// if len(tokenId) > 0 {
	// 	ruser, err = c.App.CreateUserWithToken(user, tokenId)
	// } else if len(inviteId) > 0 {
	// 	ruser, err = c.App.CreateUserWithInviteId(user, inviteId)
	// } else if c.IsSystemAdmin() {
	// 	ruser, err = c.App.CreateUserAsAdmin(user)
	// } else {
	// }

}
