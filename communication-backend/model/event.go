// Copyright (c) 2015-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

package model

import (
"encoding/json"
// "fmt"
"io"
// "net/http"
// "regexp"
// "strings"
// "unicode/utf8"
)

// const (
// 	Event_OPEN                       = "O"
// 	Event_INVITE                     = "I"
// 	Event_ALLOWED_DOMAINS_MAX_LENGTH = 500
// 	Event_COMPANY_NAME_MAX_LENGTH    = 64
// 	Event_DESCRIPTION_MAX_LENGTH     = 255
// 	Event_DISPLAY_NAME_MAX_RUNES     = 64
// 	Event_EMAIL_MAX_LENGTH           = 128
// 	Event_NAME_MAX_LENGTH            = 64
// 	Event_NAME_MIN_LENGTH            = 2
// )

type Event struct {
	Id             string     `json:"id"`
	OrganizerID    string     `json:"organizer_id"`
	EventName      string     `json:"event_name"`
	VenueID        string     `json:"venue_id"`
	StartTime      int64      `json:"start_time"`
	EndTime        int64      `json:"end_time"`
	Description    string     `json:"description"`
	Type           string     `json:"type"`
	InviteOnly     bool       `json:"invite_only"`
	EventChannelID string     `json:"event_channel_id"`
	Props          StringMap  `json:"props,omitempty"`
}

type EventPatch struct {
	StartTime   *int64    `json:"start_time"`
	EndTime     *int64    `json:"end_time"`
	EventName   *string   `json:"display_name"`
	Description *string   `json:"description"`
	Type        *string   `json:"type"`
	InviteOnly  *bool     `json:"invite_only"`
	Props       StringMap `json:"props"`
}

// type EventForExport struct {
// 	Event
// 	SchemeName *string
// }

// type Invites struct {
// 	Invites []map[string]string `json:"invites"`
// }

// func InvitesFromJson(data io.Reader) *Invites {
// 	var o *Invites
// 	json.NewDecoder(data).Decode(&o)
// 	return o
// }

// func (o *Invites) ToEmailList() []string {
// 	emailList := make([]string, len(o.Invites))
// 	for _, invite := range o.Invites {
// 		emailList = append(emailList, invite["email"])
// 	}
// 	return emailList
// }

// func (o *Invites) ToJson() string {
// 	b, _ := json.Marshal(o)
// 	return string(b)
// }

// func (o *Event) ToJson() string {
// 	b, _ := json.Marshal(o)
// 	return string(b)
// }

// func EventFromJson(data io.Reader) *Event {
// 	var o *Event
// 	json.NewDecoder(data).Decode(&o)
// 	return o
// }

// func EventMapToJson(u map[string]*Event) string {
// 	b, _ := json.Marshal(u)
// 	return string(b)
// }

// func EventMapFromJson(data io.Reader) map[string]*Event {
// 	var Events map[string]*Event
// 	json.NewDecoder(data).Decode(&Events)
// 	return Events
// }

// func EventListToJson(t []*Event) string {
// 	b, _ := json.Marshal(t)
// 	return string(b)
// }

// func EventListFromJson(data io.Reader) []*Event {
// 	var Events []*Event
// 	json.NewDecoder(data).Decode(&Events)
// 	return Events
// }

// func (o *Event) Etag() string {
// 	return Etag(o.Id, o.UpdateAt)
// }

// func (o *Event) IsValid() *AppError {

// 	if len(o.Id) != 26 {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.id.app_error", nil, "", http.StatusBadRequest)
// 	}

// 	if o.CreateAt == 0 {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.create_at.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if o.UpdateAt == 0 {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.update_at.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if len(o.Email) > Event_EMAIL_MAX_LENGTH {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.email.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if len(o.Email) > 0 && !IsValidEmail(o.Email) {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.email.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if utf8.RuneCountInString(o.DisplayName) == 0 || utf8.RuneCountInString(o.DisplayName) > Event_DISPLAY_NAME_MAX_RUNES {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.name.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if len(o.Name) > Event_NAME_MAX_LENGTH {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.url.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if len(o.Description) > Event_DESCRIPTION_MAX_LENGTH {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.description.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if IsReservedEventName(o.Name) {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.reserved.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if !IsValidEventName(o.Name) {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.characters.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if !(o.Type == Event_OPEN || o.Type == Event_INVITE) {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.type.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if len(o.CompanyName) > Event_COMPANY_NAME_MAX_LENGTH {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.company.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	if len(o.AllowedDomains) > Event_ALLOWED_DOMAINS_MAX_LENGTH {
// 		return NewAppError("Event.IsValid", "model.Event.is_valid.domains.app_error", nil, "id="+o.Id, http.StatusBadRequest)
// 	}

// 	return nil
// }

// func (o *Event) PreSave() {
// 	if o.Id == "" {
// 		o.Id = NewId()
// 	}

// 	o.CreateAt = GetMillis()
// 	o.UpdateAt = o.CreateAt

// 	if len(o.InviteId) == 0 {
// 		o.InviteId = NewId()
// 	}
// }

// func (o *Event) PreUpdate() {
// 	o.UpdateAt = GetMillis()
// }

// func IsReservedEventName(s string) bool {
// 	s = strings.ToLower(s)

// 	for _, value := range reservedName {
// 		if strings.Index(s, value) == 0 {
// 			return true
// 		}
// 	}

// 	return false
// }

// func IsValidEventName(s string) bool {

// 	if !IsValidAlphaNum(s) {
// 		return false
// 	}

// 	if len(s) < Event_NAME_MIN_LENGTH {
// 		return false
// 	}

// 	return true
// }

// var validEventNameCharacter = regexp.MustCompile(`^[a-z0-9-]$`)

// func CleanEventName(s string) string {
// 	s = strings.ToLower(strings.Replace(s, " ", "-", -1))

// 	for _, value := range reservedName {
// 		if strings.Index(s, value) == 0 {
// 			s = strings.Replace(s, value, "", -1)
// 		}
// 	}

// 	s = strings.TrimSpace(s)

// 	for _, c := range s {
// 		char := fmt.Sprintf("%c", c)
// 		if !validEventNameCharacter.MatchString(char) {
// 			s = strings.Replace(s, char, "", -1)
// 		}
// 	}

// 	s = strings.Trim(s, "-")

// 	if !IsValidEventName(s) {
// 		s = NewId()
// 	}

// 	return s
// }

// func (o *Event) Sanitize() {
// 	o.Email = ""
// }

// func (t *Event) Patch(patch *EventPatch) {
// 	if patch.DisplayName != nil {
// 		t.DisplayName = *patch.DisplayName
// 	}

// 	if patch.Description != nil {
// 		t.Description = *patch.Description
// 	}

// 	if patch.CompanyName != nil {
// 		t.CompanyName = *patch.CompanyName
// 	}

// 	if patch.AllowedDomains != nil {
// 		t.AllowedDomains = *patch.AllowedDomains
// 	}

// 	if patch.InviteId != nil {
// 		t.InviteId = *patch.InviteId
// 	}

// 	if patch.AllowOpenInvite != nil {
// 		t.AllowOpenInvite = *patch.AllowOpenInvite
// 	}
// }

// func (t *EventPatch) ToJson() string {
// 	b, err := json.Marshal(t)
// 	if err != nil {
// 		return ""
// 	}

// 	return string(b)
// }

// func EventPatchFromJson(data io.Reader) *EventPatch {
// 	decoder := json.NewDecoder(data)
// 	var Event EventPatch
// 	err := decoder.Decode(&Event)
// 	if err != nil {
// 		return nil
// 	}

// 	return &Event
// }


func EventFromJson(data io.Reader) *Event {
	var o *Event
	json.NewDecoder(data).Decode(&o)
	return o
}

func EventListToJson(t []*Event) string {
	b, _ := json.Marshal(t)
	return string(b)
}

func (o *Event) PreSave() {
	if o.Id == "" {
		o.Id = NewId()
	}

	// o.CreateAt = GetMillis()
	// o.UpdateAt = o.CreateAt

	// if len(o.InviteId) == 0 {
	// 	o.InviteId = NewId()
	// }
}

func (o *Event) ToJson() string {
	b, _ := json.Marshal(o)
	return string(b)
}
