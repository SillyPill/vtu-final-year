-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Linux (x86_64)
--
-- Host: 172.17.0.1    Database: notice_board_db
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `notices`
--

DROP TABLE IF EXISTS `notices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notices` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Designation` varchar(255) DEFAULT NULL,
  `Content` text,
  `PostedAt` datetime DEFAULT NULL,
  `Hashtags` varchar(255) DEFAULT NULL,
  `Approved` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notices`
--

LOCK TABLES `notices` WRITE;
/*!40000 ALTER TABLE `notices` DISABLE KEYS */;
INSERT INTO `notices` VALUES (8,'','batman','not_sure','## Check out my cool new website\n\n[Over Here]([like this](http://google.com))\n\n\n| Allapp   | AlApp        | [a: \\`la: p] |\n|:----------|:--------------:|:---------------------------------- |\n| *All*app | AluminiumApp | Conversations                    |\n\n**DO NOT SHOUT FIRE IN A CROWDED THEATER**\n\n![NTU](http://www.ntu.edu.sg/AboutNTU/CorporateInfo/PublishingImages/about-NTU_pic-2.jpg)\n','2018-11-13 04:41:40','',1),(9,'','batman','not_sure','Linux kernel\n============\n\nThere are several guides for kernel developers and users. These guides can\nbe rendered in a number of formats, like HTML and PDF. Please read\nDocumentation/admin-guide/README.rst first.\n\nIn order to build the documentation, use ``make htmldocs`` or\n``make pdfdocs``.  The formatted documentation can also be read online at:\n\n    https://www.kernel.org/doc/html/latest/\n\nThere are various text files in the Documentation/ subdirectory,\nseveral of them using the Restructured Text markup notation.\n\nPlease read the Documentation/process/changes.rst file, as it contains the\nrequirements for building and running the kernel, and information about\nthe problems which may result by upgrading your kernel.\n','2018-11-13 05:01:18','',0),(10,'','batman','not_sure','# Library Card Renewal\n\nThe **freshers** are hereby informed to register for their library membership at library *one*.\n\n[Click Here to navigate](https://goo.gl/maps/2WwSZ4yKKpG2)\n\n','2018-11-13 08:41:07','',1),(11,'','batman','not_sure','## Schedule for today\'s Singapore-India Hackathon\n\n| Time     | Agenda              | Venue              |\n| ----     | -----               | ------             |\n| 9:00 am  | Hackathon Continues | Function Hall 1/2  |\n| 10:30 am | Refreshments        | Outside Auditorium |\n| 10:45 am | Hackathon Resumes   | -                  |\n| 12:30 pm | Lunch               | Outside Auditorium |\n| 4:00 pm  | Refreshments        | Outside Auditorium |\n| 7:00 pm  | Dinner              | Ourside Auditorium |\n| 9:00 pm  | Hackathon Ends      | -                  |\n\n','2018-11-13 09:36:27','',0),(12,'','batman','not_sure','## Schedule for today\'s Singapore-India Hackathon\n\n| Time     | Agenda              | Venue              |\n| ----     | -----               | ------             |\n| 9:00 am  | Hackathon Continues | Function Hall 1/2  |\n| 10:30 am | Refreshments        | Outside Auditorium |\n| 10:45 am | Hackathon Resumes   | -                  |\n| 12:30 pm | Lunch               | Outside Auditorium |\n| 4:00 pm  | Refreshments        | Outside Auditorium |\n| 7:00 pm  | Dinner              | Ourside Auditorium |\n| 9:00 pm  | Hackathon Ends      | -                  |\n\n','2018-11-13 09:36:51','',1),(13,'','batman','student','## Coming Soon..\n\n![Awesome Work coming soon](https://s18670.pcdn.co/wp-content/uploads/Clipboard.jpg)\n','2018-11-13 09:57:16','',1);
/*!40000 ALTER TABLE `notices` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-24 13:19:49
